'use strict';

var AWS = require('aws-sdk');
var s3 = new AWS.S3();

const signedUrlExpireSeconds = 60 * 5

module.exports.signed_url = (event, context, callback) => {
  const s3Obj = event.Records[0].s3

  const bucketParam = s3Obj.bucket.name
  const keyParam = s3Obj.object.key

  var params = {
    Bucket: bucketParam,
    Key: keyParam,
    Expires: signedUrlExpireSeconds
  };

  const url = s3.getSignedUrl('getObject', params)
  console.log(url)

  const response = { statusCode: 200 };
  callback(null, response);
};
