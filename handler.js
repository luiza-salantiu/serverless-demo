'use strict';

var AWS = require('aws-sdk');
var s3 = new AWS.S3();

module.exports.hello = (event, context, callback) => {
  var params = {
    Bucket: 'serverless-tutorial-techgeeks-12345',
  };

  s3.listObjectsV2(params, function(err, data) {
    if (err) {
      console.log(err, err.stack);
    } else {
      const response = {
        statusCode: 200,
        body: JSON.stringify({
          "bucket_list": data
        }),
      };
      callback(null, response);
    }
  });
};
